// Node.js Routing with HTTP Methods

//	CRUD Operations			HTTP Methods
//	C - create				POST
//	R - read				GET
//	U - update				PUT / PATCH
//	D - delete				DELETE

const http = require('http');

// Mock Data for Users and Courses

let users = [
	{
		username : "peterIsHomeless",
		email : "peterParker@mail.com",
		password : "peterNoWayHome"
	},
	{
		username : "Tony3000",
		email : "starksIndustries@gmail.com",
		password : "ironManWillBeBack"
	}
]

let courses = [
	{
		name : "Math 103",
		price : 2500,
		isActive : true
	},
	{
		name : "Biology 201",
		price : 2500,
		isActive : true
	}
];

http.createServer((req, res) => {

	if(req.url === "/" && req.method === "GET"){
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end("This route is for checking a GET method.");

	} else if (req.url === "/" && req.method === "POST"){
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is for checking a POST method.');

	} else if (req.url === "/" && req.method === "PUT"){
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end('This route is for checking a PUT method.');

	} else if (req.url === "/" && req.method === "DELETE"){
		res.writeHead(200, {'Content-Type' : 'text/plain'});
		res.end("This is for checking a DELETE method.");

	} else if (req.url === "/users" && req.method === "GET"){
		// Change the value of Content-Type header if we are passing json as our server response: 'application/json'.
		res.writeHead(200, {'Content-Type' : 'application/json'});
		// We cannot pass other data types as a response except for strings
		// To be able to pass the array of users, first we stringify the array as JSON.
		res.end(JSON.stringify(users));

	} else if (req.url === "/courses" & req.method === "GET"){
		res.writeHead(200, {'Content-Type' : 'application/json'});
		res.end(JSON.stringify(courses));

	} else if (req.url === "/users" && req.method === "POST"){
		let requestBody = "";

		// Receiving data from client to nodejs server requires 2 steps:

		// data step - This part will read the stream of data from our client and process the incoming data into the requestBody variable. ( 'data' needs to be the first argument. It's like following syntax.)
		
		req.on('data', (data) => {
			console.log(data.toString())
			requestBody += data
		});

		// end step - This will run once or after the request has been completely sent from our client.
		
		req.on('end', () => {
			console.log(requestBody);
			requestBody = JSON.parse(requestBody);
			let newUser = {
				username : requestBody.username,
				email : requestBody.email,
				password : requestBody.password
			};
			users.push(newUser);
			console.log(users);

			res.writeHead(200, {'Content-Type' : 'application/json'});
			res.end(JSON.stringify(users))
		});
	} else if (req.url === "/courses" && req.method === "POST"){
		let requestBody = "";

		req.on('data', (data) => {
			requestBody += data;
		});
		req.on('end', () => {
			requestBody = JSON.parse(requestBody);

			let newCourse = {
				name : requestBody.name,
				price : requestBody.price,
				isActive : requestBody.isActive
			};

			courses.push(newCourse);

			res.writeHead(200, {'Content-Type' : 'application/json'});
			res.end(JSON.stringify(courses));
		});
	};

}).listen(4000);
console.log('Server is running on localhost:4000.');
